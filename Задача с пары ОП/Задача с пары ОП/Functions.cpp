#include "Functions.h"
#include <chrono>
using namespace std;

// ������� ��� ������ ������� 
chrono::high_resolution_clock::time_point sstart;
void tic()
{
	sstart =chrono:: high_resolution_clock::now();
}

long toc()
{
	return chrono::duration_cast<chrono::milliseconds>(chrono::high_resolution_clock::now() - sstart).count();
}

// ������� ��� ������ ������� � �������
void printArr(int* array, int size) {
	for (int i = 0; i < size; i++) printf("%d ", array[i]); // C�-���� �����
	// cout << array[i] << " "; // �++ �����
}

void printArrReverse(int* array, int size) {
	for (int i = size - 1; i >= 0; i--) printf("%d ", array[i]); // C�-���� �����
	//cout << array[i] << " "; // �++ �����
}

// �����������
void swap(int& a, int& b)
{
	int buf = a;
	a = b;
	b = buf;
	return;
}

bool myNextPermutation(int* array, int size)
{
	if (size <= 1) return false;

	int i = size - 1;
	while (1) // �������� ����
	{
		int key = i;
		i--;

		if (array[i] < array[key])
		{
			int j = size;
			while (!(array[i] < array[--j])); // ������� (������ ������) ���� ������� ����� ���������, ��� ����� < ������� 
			swap(array[i], array[j]);
			reverse(&array[key], array + size);
			return true;
		}

		if (i == 0) // ���� ����� �� ������, �� ������ ��� ������������ ��������� (���������� )
		{
			reverse(array, array + size);
			return false;
		}

	} // ����� ��������� �����

}

// �������� �������, �������� ������
void alternative(int length, int nGroups) // ���������� NextPermutation �� �������
{
	tic();
	int* array = new int[length];	// ������ � ���� ������������������ 
	int n = length / nGroups;		// ���������� ���������� ��������� ������ �������� � ������������������ 
	int total = 0;					// ����� ���������� ������������������� 
	for (int i = 0; i < length; i++)
	{
		array[i] = i / n;
	}
	do {
		total++;
		printArr(array, length);
		cout << endl;
	} while (next_permutation(array, array + length)); // ���� ��� ��������, ������ �� ������� array 
													   // ��������� �� ����������� ������� ������������
	cout << "Total alternative: " << total << endl;
	delete[] array;
	long ms = toc();
	cout << "Alternative Time:  " << ms << " ms" << endl;
	return;
}

void myAlternative(int length, int nGroups) // ���������� myNextPermutation
{
	tic();
	int* array = new int[length];	// ������ � ���� ������������������ 
	int n = length / nGroups;		// ���������� ���������� ��������� ������ �������� � ������������������ 
	int total = 0;					// ����� ���������� ������������������� 
	for (int i = 0; i < length; i++)
	{
		array[i] = i / n;
	}
	do {
		total++;
		printArr(array, length);
		cout << endl;
	} while (myNextPermutation(array, length)); // ���� ��� ��������, ������ �� ������� array 
												// ��������� �� ����������� ������� ������������
	cout << "Total My alternative: " << total << endl;
	delete[] array;
	long ms = toc();
	cout << "My Alternative Time:  " << ms << " ms" << endl;
	return;
}

void myBadFunc(int length, int nGroups) // ������ ������� ������������������� ����� length 
										// (��������� �� nGroups ������ ���������) � ����� 
										// ���������� 
{
	tic();
	int total = 0; // ����� ���������� ������������������� 
	int n = length / nGroups;  // ���������� ���������� ��������� ������ �������� � ������������������ 
	int* array = new int[length]; // ������ � ���� ����� � nGroups ������� ����������
	int* numVal = new int[nGroups]; // � i ������ ������ ���������� ��������� � ������������������ �� ��������� i
	fill(numVal, numVal + nGroups, 0);
	fill(array, array + length, 0);

	for (int i = 0; i < pow((double)nGroups, (double)length); i++)
	{
		int iter = 0;
		while (iter<length) // ���������� ������� � �� nGroup � ����� � ������� m
		{
			if (array[iter] + 1 < nGroups)
			{
				array[iter]++;
				break;
			}
			else
			{
				array[iter] = 0;
				iter++;
			}
		}
		iter = 0;

		for (int j = 0; j < length; j++) // ������� ���������� ������� ��������
		{
			numVal[array[j]]++;
		}

		int isGoodPerm = 1;
		for (int j = 0; j<nGroups; j++) //���������, ���������� �� �� ���������� 
		{
			if (numVal[j] != n) isGoodPerm = 0;
		}
		fill(numVal, numVal + nGroups, 0);
		if (isGoodPerm)// ���� ������������������ ��� ���������� 
		{
			total++;
			printArrReverse(array, length); // ������� ������������������ 
			cout << endl;
		}
		isGoodPerm = 1;
	}

	cout << "Total: " << total << endl;
	delete[] numVal;
	delete[] array;
	long ms = toc();
	cout << "My Function Time:  " << ms << " ms" << endl;
	return;
}



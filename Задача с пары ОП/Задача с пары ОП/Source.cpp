#include <math.h>
#include "Functions.h"
using namespace std;

int main()
{
	int nGroups;
	int length;
	cout << "Write total number of different elements (Ng): " << endl;
	cin >> nGroups;
	cout << "Write length (Nb): " << endl;
	cin >> length;
	

	myBadFunc(length, nGroups);
	cout << endl;

	alternative(length, nGroups);
	cout << endl;

	myAlternative(length, nGroups);
	cout << endl;
	
	system("pause");
	return 0;
}
#include <functional>
#include <iostream>
#include <algorithm>  
// ������� ��� ������ ������� 
void tic();
long toc();

// ������� ��� ������ ������� � �������
void printArr(int* array, int size);
void printArrReverse(int* array, int size);

// �����������
void swap(int& a, int& b);
bool myNextPermutation(int* array, int size);

// �������� �������, �������� ������
void alternative(int length, int nGroups);
void myAlternative(int length, int nGroups);
void myBadFunc(int length, int nGroups);

